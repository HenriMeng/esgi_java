package Units;

public class Phi {

    int value;

    public Phi(int value) throws Exception {
        if (isValid(value))
            this.value = value;
        else
            throw new Exception("value != -90 <-> 90");
    }

    public boolean isValid(int value) {
        return (value >= -90 && value <= 90);
    }

    public int getValue() {
        return value;
    }
}
